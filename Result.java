package de.czoch.aufgaben_packages_immer_klein.league;

public class Result {


    private int goalsHomeTeam;
    private int goalsGuestTeam;
    private int pointsHomeTeam;
    private int pointsGuestTeam;

    public Result(int goalsHomeTeam, int goalsGuestTeam) {
        this.goalsHomeTeam = goalsHomeTeam;
        this.goalsGuestTeam = goalsGuestTeam;
        setPoints();
    }

    private void setPoints(){
        if(goalsHomeTeam - goalsGuestTeam < 0){
            pointsHomeTeam = 0;
            pointsGuestTeam = 3;
        }
        else if(goalsHomeTeam - goalsGuestTeam > 0){
            pointsHomeTeam = 3;
            pointsGuestTeam = 0;
        }
        else{
            pointsHomeTeam = pointsGuestTeam = 1;
        }
    }

    public int getGoalsHomeTeam() {
        return goalsHomeTeam;
    }

    public int getGoalsGuestTeam() {
        return goalsGuestTeam;
    }

    public int getPointsHomeTeam() {
        return pointsHomeTeam;
    }

    public int getPointsGuestTeam() {
        return pointsGuestTeam;
    }

    @Override
    public String toString() {
        return "(" + goalsHomeTeam +
                " : " + goalsGuestTeam;
    }
}
