package de.czoch.aufgaben_packages_immer_klein.league;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class League {
    private List<Team> league = new ArrayList<>();
    private String name;

    public League(String name, Team... teams){
        this.name = name;
        league.addAll(Arrays.asList(teams));
    }

    public List<Team> getTeams() {
        return league;
    }

    @Override
    public String toString() {
        String out = String.format("%s",name);
        for(Team team : league){
            out.concat(team.toString());
        }
        return out;
    }
}
