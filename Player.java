package de.czoch.aufgaben_packages_immer_klein.league;

public class Player {
    private String name;
    private Skill defense;
    private Skill attack;

    public Player(String name, Skill attack, Skill defense){
        setName(name);
        setAttack(attack);
        setDefense(defense);
    }

    @Override
    public String toString() {
        return String.format("Name: %s\nAttack: %s\nDefense: %s\n",getName(),attack.toString(),defense.toString());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Skill getDefense() {
        return defense;
    }

    public void setDefense(Skill defense) {
        this.defense = defense;
    }

    public Skill getAttack() {
        return attack;
    }

    public void setAttack(Skill attack) {
        this.attack = attack;
    }
}
