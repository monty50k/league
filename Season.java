package de.czoch.aufgaben_packages_immer_klein.league;

import java.util.LinkedList;
import java.util.List;

public class Season {
    private League league;
    private List<Game> season = new LinkedList<>();

    public Season(League league){
        this.league = league;
        play();
    }

    private void play(){
        for(Team homeTeam : league.getTeams()){
            for(Team guestTeam : league.getTeams()){
                if(homeTeam != guestTeam){
                    season.add(new Game(homeTeam,guestTeam));
                }
            }
        }
    }

    public void games(){
        for(Game game : season){
            System.out.println(game);
        }
    }


    public void table() {
        for(Team team : league.getTeams()){
            int points = 0;
            int goalsOut = 0;
            int goalsIn = 0;
            for(Game game : season){
                if(team == game.getHomeTeam()){
                    points += game.getResult().getPointsHomeTeam();
                    goalsOut += game.getResult().getGoalsHomeTeam();
                    goalsIn += game.getResult().getGoalsGuestTeam();
                }
                if(team == game.getGuestTeam()){
                    points += game.getResult().getPointsGuestTeam();
                    goalsOut += game.getResult().getGoalsGuestTeam();
                    goalsIn += game.getResult().getGoalsHomeTeam();
                }
            }
            System.out.printf("%-15s Points: %d Goals shot: %d Goals eaten: %d\n",team.getName(),points,goalsOut,goalsIn);
        }
    }
}
