package de.czoch.aufgaben_packages_immer_klein.league;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Team {

    private String name;
    private List<Player> team = new ArrayList();

    public Team(String name, Player... players){
        this.name = name;
        this.team.addAll(Arrays.asList(players));
    }

    public String getName(){
        return name;
    }



    public double attack(){
        double attack = 0;
        for(Player player : team){
            attack += player.getAttack().getValue();
        }
        attack /= team.size();
        return attack;
    }

    public double defend(){
        double defend = 0;
        for(Player player : team){
            defend += player.getDefense().getValue();
        }
        defend /= team.size();
        return defend;
    }

    @Override
    public String toString() {
        String out = String.format("Team %s\n",name);
        for(Player player : team){
            out.concat(player.toString());
        }
        return out;
    }
}
