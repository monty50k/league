package de.czoch.aufgaben_packages_immer_klein.league;

public class Main {
    public static void main(String[] args) {

        Player helmut = new Player("Helmut",Skill.POOR,Skill.WORST);
        Player herbert = new Player("Herbert",Skill.AVERAGE,Skill.POOR);
        Player heinrich = new Player("Heinrich",Skill.POOR,Skill.AVERAGE);

        Team humpel = new Team("Humpel", helmut, heinrich, herbert);

        Player elsa = new Player("Elsa",Skill.GOOD,Skill.AVERAGE);
        Player helga = new Player("Helga", Skill.AVERAGE,Skill.GOOD);
        Player erna = new Player("Erna", Skill.AVERAGE,Skill.AVERAGE);

        Team cow = new Team("Cow",elsa,helga,erna);

        Player cptTsubasa = new Player("Captain Tsubasa",Skill.MASTER,Skill.GOOD);
        Player pele = new Player("Pelé",Skill.MASTER,Skill.GOOD);
        Player franzl = new Player("Kaiser Franz",Skill.GOOD,Skill.MASTER);

        Team allStars = new Team("Allstars",cptTsubasa,pele,franzl);

        League javaleague = new League("Javaleague",humpel,cow,allStars);
        Season one = new Season(javaleague);
        one.table();
        one.games();


    }
}
