package de.czoch.aufgaben_packages_immer_klein.league;

public enum Skill {
WORST(0), POOR(1), AVERAGE(2), GOOD(3), MASTER(4);

    private int value;


    Skill(int value) {
        this.value = value;
    }

    public int getValue(){
        return value;
    }

    @Override
    public String toString() {
        return super.toString() + "(" + value + ")";
    }
}
