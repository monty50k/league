package de.czoch.aufgaben_packages_immer_klein.league;

import java.util.Random;

public class Game {
    private Team homeTeam;
    private Team guestTeam;
    private Result result;

    public Game(Team homeTeam, Team guestTeam){
    setHomeTeam(homeTeam);
    setGuestTeam(guestTeam);
    match();
    }

    private void match(){
        Random r = new Random();
        double attacksHomeTeam = homeTeam.attack() * (r.nextGaussian()*20+50)/50 ;
        double attacksGuestTeam = guestTeam.attack() * (r.nextGaussian()*20+50)/50;
        double defendsHomeTeam = homeTeam.defend();
        double defendsGuestTeam = guestTeam.defend();
        int goalsHomeTeam = (int) Math.round(attacksHomeTeam - defendsGuestTeam);
        int goalsGuestTeam = (int) Math.round(attacksGuestTeam - defendsHomeTeam);
        if(goalsHomeTeam < 0){
            goalsHomeTeam = 0;
        }
        if(goalsGuestTeam < 0){
            goalsGuestTeam = 0;
        }
        this.result = new Result(goalsHomeTeam,goalsGuestTeam);

    }

    public Result getResult() {
        return result;
    }

    public String toString(){
        return String.format("%s vs %s (%d : %d)",homeTeam.getName(),guestTeam.getName(),result.getGoalsHomeTeam(),result.getGoalsGuestTeam());
    }

    public Team getHomeTeam() {
        return homeTeam;
    }

    public void setHomeTeam(Team homeTeam) {
        this.homeTeam = homeTeam;
    }

    public Team getGuestTeam() {
        return guestTeam;
    }

    public void setGuestTeam(Team guestTeam) {
        this.guestTeam = guestTeam;
    }


}
